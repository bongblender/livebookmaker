extends Control


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	drag_non_placed_image()
	pass


var button_open: bool = false
func _on_summon_color_palate_pressed():
	if button_open == false:
		$AnimationPlayer.play("button_goes_away")
		button_open = true
	elif button_open == true:
		$AnimationPlayer.play_backwards("button_goes_away")
		button_open = false
	pass # Replace with function body.



func _creat_new_image():
	$place_image.visible = true
	$place_image.position = Vector2(200.0, 200.0)
	pass

func _on_texture_button_pressed():
	_creat_new_image()
	pass # Replace with function body.


func _on_h_slider_value_changed(value):
	$place_image/VBoxContainer/TextureRect.scale = Vector2(value, value)
	print($place_image/VBoxContainer/TextureRect.global_position)
	pass # Replace with function body.

var dragging : bool = false
func _on_texture_rect_mouse_entered():
	dragging = true
func _on_texture_rect_mouse_exited():
	dragging = false

func _on_color_rect_mouse_entered():
	dragging = true
	pass # Replace with function body.
func _on_color_rect_mouse_exited():
	dragging = false
	pass # Replace with function body.


func drag_non_placed_image():
	if dragging == true:
		if Input.is_action_pressed("ui_left"):
			var mouse_posi = get_global_mouse_position()
			if $place_image.visible == true:
				$place_image.position = Vector2(mouse_posi.x - ($place_image.size.x/2), mouse_posi.y - ($place_image.size.y/2))
			else :
				pass
			if $place_text.visible == true:
				$place_text.position = Vector2(mouse_posi.x - 20.0, mouse_posi.y - ($place_text.size.y/2 + 40.0))
			else :
				pass
			print(mouse_posi)
		else :
			pass


func _on_no_pressed():
	$place_image.visible = false
	pass # Replace with function body.
func _on_yes_pressed(): 
	var new_image_scale = $place_image/VBoxContainer/TextureRect.scale
	var new_image_position = $place_image/VBoxContainer/TextureRect.global_position
	var new_image_texture = $place_image/VBoxContainer/TextureRect.texture # need manual texture path
	var texture_new = TextureRect.new()
	texture_new.position = new_image_position
	texture_new.scale = new_image_scale
	texture_new.texture = new_image_texture
	print(new_image_texture)
	$animetion_canvas.add_child(texture_new)
	$place_image.visible = false


func _on_width_control_value_changed(value):
	$place_text/VBoxContainer/TextEdit.custom_minimum_size.x = value
	pass # Replace with function body.


func _on_height_control_value_changed(value):
	$place_text/VBoxContainer/TextEdit.custom_minimum_size.y = value
	pass # Replace with function body.


func _on_text_size_value_changed(value):
	$place_text/VBoxContainer/TextEdit.set("theme_override_font_sizes/font_size", value)
	pass # Replace with function body.

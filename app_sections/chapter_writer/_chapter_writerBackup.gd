extends Control



var action_card_instance = null


# Called when the node enters the scene tree for the first time.
func _ready(): # Replace with function body.
	_lane_creation()
	pass
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	_new_action_card_control()

var on_area: bool = false
var on_top_of: int
var watch_path: bool = false
var _active_card_control: bool = false
var the_blank_place = null
func _new_action_card_control():
	if Input.is_action_pressed("ui_left"):
		if action_card_instance == null:
			if _active_card_control != false:
				_creat_action_card()
			else :
				pass
		if action_card_instance != null:
			_card_positon_update()
			watch_path = true
		else :
			watch_path = false
			pass
	else :
		if action_card_instance != null:
			_remove_card()
		if on_area:
			if the_blank_place != null:
				if card_selected == "red":
					print(the_blank_place)
					the_blank_place.texture = load("res://app_sections/chapter_writer/_textures/bitmap_rect1.png")
					blank_cards[on_top_of] = card_selected
					the_blank_place = null
				elif card_selected == "blue":
					print(the_blank_place)
					the_blank_place.texture = load("res://app_sections/chapter_writer/_textures/bitmap_rect3.png")
					blank_cards[on_top_of] = card_selected
					the_blank_place = null
				else :
					pass
				print(blank_cards)
			else :
				pass
		watch_path = false


var card_selected:String = ""
func _creat_action_card():
	action_card_instance = Sprite2D.new()
	var card_texture: String
	if card_selected == "red":
		card_texture = "res://app_sections/chapter_writer/_textures/bitmap_rect1.png"
		action_card_instance.texture = load(card_texture)
		$".".add_child(action_card_instance)
	elif card_selected == "blue":
		card_texture = "res://app_sections/chapter_writer/_textures/bitmap_rect3.png"
		action_card_instance.texture = load(card_texture)
		$".".add_child(action_card_instance)
	else :
		pass
	
func _card_positon_update():
	action_card_instance.position = get_global_mouse_position()
func _remove_card():
	action_card_instance.queue_free()
	action_card_instance = null

func _on_texture_button_mouse_entered():
	_active_card_control = true
	card_selected = "red"
	pass # Replace with function body.
func _on_texture_button_mouse_exited():
	_active_card_control = false
	pass # Replace with function body.

func _on_texture_button_2_mouse_entered():
	_active_card_control = true
	card_selected = "blue"
	pass # Replace with function body.
func _on_texture_button_2_mouse_exited():
	_active_card_control = false
	pass # Replace with function body.
	
	
	
	
var blank_cards = ["none", "none", "blue"]
var sprite_path: String = ""
@onready var upper_lane = $action_series_panel/action_series_ScCont/action_series_VCont/upper_lane
@onready var lower_lane = $action_series_panel/action_series_ScCont/action_series_VCont/lower_lane
func _lane_creation():
	for child in $action_series_panel/action_series_ScCont/action_series_VCont/upper_lane.get_children():
		child.queue_free()
	for i in range(0, len(blank_cards)):
		_creat_blank_card(i, blank_cards[i])
		
func _creat_blank_card(serial, card_type):
	var color_rect = ColorRect.new()
	#color_rect.name = "color_rect_place" + "_" + str(serial)
	var _sprite = Sprite2D.new()
	#_sprite.name = "card_place" + "_" + str(serial)
	var _intigreted_edit_button = TextureButton.new()
	color_rect.custom_minimum_size = Vector2(140.0, 140.0)
	color_rect.color = Color("87e3f95b")
	color_rect.size_flags_horizontal = Control.SIZE_SHRINK_CENTER
	color_rect.size_flags_vertical = Control.SIZE_SHRINK_CENTER
	color_rect.add_child(_sprite)
	_sprite.position = Vector2(70.0, 70.0)
	if card_type == "red":
		_sprite.texture = load("res://app_sections/chapter_writer/_textures/bitmap_rect1.png")
	elif card_type == "blue":
		_sprite.texture = load("res://app_sections/chapter_writer/_textures/bitmap_rect3.png")
	else :
		pass
	_intigreted_edit_button.texture_hover = load("res://app_sections/chapter_writer/_textures/bitmap_rect311.png")
	_intigreted_edit_button.texture_pressed = load("res://app_sections/chapter_writer/_textures/bitmap_rect312.png")
	_intigreted_edit_button.modulate = Color("ffffff50")
	_intigreted_edit_button.connect("mouse_entered", _set_placement_parameter.bind(serial, _sprite))
	_intigreted_edit_button.connect("pressed", _edit_card.bind(serial))
	color_rect.add_child(_intigreted_edit_button)
	upper_lane.add_child(color_rect)
	
func _set_placement_parameter(serial, object):
	on_area = true
	if watch_path == true:
		the_blank_place = object
	else :
		pass
	on_top_of = serial
func _edit_card(card_number):
	print(card_number)


func _on_texture_button_3_pressed():
	blank_cards.append("none")
	_lane_creation()
	pass # Replace with function body.

extends Control

func _ready():
	build_action_card_rack_to_pickup()
	_create_lane()
	pass

func _process(_delta):
	_update_dragging_card_position()
	pass

#this variable determine placement of cards on lane
var emty_lane_data = [
	["", ""],
	["blue", "red"],
	["blue", "red"],
	["red", "red"],
	["blue", "red"],
]
#creating those lane with for loop and emty_lane_data
@onready var upper_lane = $action_series_panel/action_series_ScCont/action_series_VCont/upper_lane
@onready var lower_lane = $action_series_panel/action_series_ScCont/action_series_VCont/lower_lane
var box_hover_texture = load("res://app_sections/chapter_writer/_textures/bitmap_rect311.png")
var box_click_texture = load("res://app_sections/chapter_writer/_textures/bitmap_rect312.png")
func _create_lane():
	for upper_lane_children in upper_lane.get_children():
		upper_lane_children.queue_free()
	for lower_lane_children in lower_lane.get_children():
		lower_lane_children.queue_free()
	for i in range(0, len(emty_lane_data)):
		var upper_lane_type = emty_lane_data[i][0]
		var lower_lane_type = emty_lane_data[i][1]
		
		#upper lane
		var upper_lane_box = TextureRect.new()
		var texture_for_upper_lane: String
		if upper_lane_type == "":
			texture_for_upper_lane = "res://app_sections/chapter_writer/_textures/bitmap_rect311.png"
		elif upper_lane_type == "blue":
			texture_for_upper_lane = "res://app_sections/chapter_writer/_textures/bitmap_rect3.png"
		elif upper_lane_type == "red":
			texture_for_upper_lane = "res://app_sections/chapter_writer/_textures/bitmap_rect1.png"
		upper_lane_box.texture = load(texture_for_upper_lane)
		upper_lane_box.size_flags_horizontal = Control.SIZE_SHRINK_CENTER
		upper_lane_box.size_flags_vertical = Control.SIZE_SHRINK_CENTER
		var new_upper_button = TextureButton.new()
		new_upper_button.texture_hover = box_hover_texture
		new_upper_button.texture_pressed = box_click_texture
		new_upper_button.modulate = Color("ffffff64")
		new_upper_button.connect("mouse_entered", _mouse_on_box.bind(upper_lane_box, [i, 0]))
		new_upper_button.connect("mouse_exited", _mouse_off_box)
		new_upper_button.connect("pressed", _edit_action_button_pressed.bind([i, 0]))
		upper_lane_box.add_child(new_upper_button)
		upper_lane.add_child(upper_lane_box)
		
		#lower lane
		var lower_lane_box = TextureRect.new()
		var texture_for_lower_lane: String
		if lower_lane_type == "":
			texture_for_lower_lane = "res://app_sections/chapter_writer/_textures/bitmap_rect311.png"
		elif lower_lane_type == "blue":
			texture_for_lower_lane = "res://app_sections/chapter_writer/_textures/bitmap_rect3.png"
		elif lower_lane_type == "red":
			texture_for_lower_lane = "res://app_sections/chapter_writer/_textures/bitmap_rect1.png"
		lower_lane_box.texture = load(texture_for_lower_lane)
		lower_lane_box.size_flags_horizontal = Control.SIZE_SHRINK_CENTER
		lower_lane_box.size_flags_vertical = Control.SIZE_SHRINK_CENTER
		var new_lower_button = TextureButton.new()
		new_lower_button.texture_hover = box_hover_texture
		new_lower_button.texture_pressed = box_click_texture
		new_lower_button.modulate = Color("ffffff64")
		new_lower_button.connect("mouse_entered", _mouse_on_box.bind(lower_lane_box, [i, 1]))
		new_lower_button.connect("mouse_exited", _mouse_off_box)
		new_lower_button.connect("pressed", _edit_action_button_pressed.bind([i, 1]))
		lower_lane_box.add_child(new_lower_button)
		lower_lane.add_child(lower_lane_box)

# here its determine if mouse on the any box of any lane, if yes which one is it...
var on_the_box = null
var on_box: bool
var _box_position: Array
func _mouse_on_box(the_box, box_position):
	on_the_box = the_box
	_box_position = box_position
	on_box = true
func _mouse_off_box():
	on_box = false
func _edit_action_button_pressed(card_link):
	print(card_link)
	pass
#here card_type is determine which action you choose , and with that through _input even you place card on box..
var card_type: String
func _change_card_on_release():
	var selected_texture: String
	if card_type == "red":
		selected_texture = "res://app_sections/chapter_writer/_textures/bitmap_rect1.png"
		on_the_box.texture = load(selected_texture)
	elif card_type == "blue":
		selected_texture = "res://app_sections/chapter_writer/_textures/bitmap_rect3.png"
		on_the_box.texture = load(selected_texture)
	else :
		if on_box == false:
			card_type = ""
func _change_data_on_release():
	emty_lane_data[_box_position[0]][_box_position[1]] = card_type
	#print(emty_lane_data)
	pass
func _input(_event):
	if Input.is_action_pressed("ui_left"):
		pass
	else :
		if dragging_card != null:
			if on_box == true:
				_change_card_on_release()
				_change_data_on_release()
			else :
				pass


var card_type_to_build: Array = [
	{"card_type": "red", "normal": "res://app_sections/chapter_writer/_textures/bitmap_rect1.png", "pressed": "res://app_sections/chapter_writer/_textures/bitmap_rect311.png", "hover": "res://app_sections/chapter_writer/_textures/bitmap_rect312.png"},
	{"card_type": "blue", "normal": "res://app_sections/chapter_writer/_textures/bitmap_rect3.png", "pressed":"res://app_sections/chapter_writer/_textures/bitmap_rect312.png" , "hover": "res://app_sections/chapter_writer/_textures/bitmap_rect311.png"}
]
@onready var action_card_rack = get_node("action_card/action_card_ScrollContainer/action_card_rack")
func build_action_card_rack_to_pickup():
	for i in range(0, len(card_type_to_build)):
		var card_info = card_type_to_build[i]
		var new_button = TextureButton.new()
		new_button.texture_normal = load(card_info["normal"])
		new_button.texture_pressed = load(card_info["pressed"])
		new_button.texture_hover = load(card_info["hover"])
		new_button.size_flags_horizontal = Control.SIZE_SHRINK_CENTER
		new_button.size_flags_vertical = Control.SIZE_SHRINK_CENTER
		new_button.connect("button_down", _new_card_button_pressed.bind(card_info["normal"], card_info["card_type"]))
		action_card_rack.add_child(new_button)

@onready var root = get_node(".")
var dragging_card = null
func _new_card_button_pressed(_card_texture, _card_type):
	dragging_card = Sprite2D.new()
	dragging_card.texture = load(_card_texture)
	dragging_card.position = get_global_mouse_position()
	card_type = _card_type
	root.add_child(dragging_card)

func _update_dragging_card_position():
	if dragging_card != null:
		if Input.is_action_pressed("ui_left"):
			dragging_card.position = get_global_mouse_position()
		else :
			dragging_card.queue_free()
			

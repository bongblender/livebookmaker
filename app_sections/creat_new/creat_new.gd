extends Control
var save_path: String = "res://data_center/menu_data/menu_data.json"

@onready var _book_name = $_book_name
@onready var _author_name = $_author_creator
@onready var _first_chapter = $_first_chapter

func _ready():
	if FileAccess.file_exists(save_path):
		var save_data = FileAccess.open(save_path, FileAccess.READ)
		var data = JSON.parse_string(save_data.get_as_text()) # Returns null if parsing failed.
		_book_name.text = data["_book_name"]
		_author_name.text = data["_author_name"]
		_first_chapter.text = data["_menu"][0]
	else :
		pass


func _creat_new_project():
	var save_data: Dictionary = {
		"_book_name": "",
		"_author_name": "",
		"_menu": [
			
		]
	}
	if not FileAccess.file_exists(save_path):
		save_data["_book_name"] = _book_name.text
		save_data["_author_name"] = _author_name.text
		save_data["_menu"].insert(0, _first_chapter.text)
		var file = FileAccess.open(save_path, FileAccess.WRITE)
		var json_string = JSON.stringify(save_data, "\t")
		file.store_string(json_string)
	else :
		save_data["_book_name"] = _book_name.text
		save_data["_author_name"] = _author_name.text
		save_data["_menu"].insert(0, _first_chapter.text)
		var file = FileAccess.open(save_path, FileAccess.WRITE)
		var json_string = JSON.stringify(save_data, "\t")
		file.store_string(json_string)


func _on__ok_pressed():
	_creat_new_project()
	get_tree().change_scene_to_file("res://app_sections/menu/menu.tscn")
	pass # Replace with function body.

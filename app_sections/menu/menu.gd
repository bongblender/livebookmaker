extends Control



var manu_save_path = "res://data_center/menu_data/menu_data.json"
var manu_data: Dictionary
# Called when the node enters the scene tree for the first time.
func _ready():
	if FileAccess.file_exists(manu_save_path):
		var save_data = FileAccess.open(manu_save_path, FileAccess.READ)
		var data = JSON.parse_string(save_data.get_as_text()) # Returns null if parsing failed.
		manu_data = data
		_creat_list()

func _edit_chapter(chapter_name, chapter_serial):
	print(chapter_name)
	var current_file_path = "res://data_center/chapter_data/chapter"+ str(chapter_serial) +".json"
	if not FileAccess.file_exists(current_file_path):
		var blank_dictonary = {
			"_chapter_name": chapter_name,
			"_chapter_serial": chapter_serial,
		}
		var file = FileAccess.open(current_file_path, FileAccess.WRITE)
		var json_string = JSON.stringify(blank_dictonary, "\t")
		file.store_string(json_string)
		GlobVar.selected_chapter_serial = chapter_serial
	else :
		GlobVar.selected_chapter_serial = chapter_serial
	get_tree().change_scene_to_file("res://app_sections/chapter_writer/_chapter_writer.tscn")

func _build_list_unite(lable_text: String, serial:int):
	var h_box_cont = HBoxContainer.new()
	h_box_cont.custom_minimum_size.x = 1060.0
	
	var list_lable = RichTextLabel.new()
	list_lable.text = lable_text
	list_lable.fit_content = true
	list_lable.autowrap_mode = TextServer.AUTOWRAP_WORD
	list_lable.custom_minimum_size.x = 1000.0
	list_lable.size_flags_horizontal = Control.SIZE_EXPAND_FILL
	list_lable.set("theme_override_fonts/normal_font", load("res://app_sections/menu/TrashHandfont.TTF"))
	list_lable.set("theme_override_font_sizes/normal_font_size", 30)
	h_box_cont.add_child(list_lable)
	
	var procede_button = Button.new()
	procede_button.custom_minimum_size.x = 50.0
	procede_button.custom_minimum_size.y = 50.0
	procede_button.size_flags_vertical = Control.SIZE_SHRINK_BEGIN
	procede_button.connect("pressed", _edit_chapter.bind(lable_text, serial))
	h_box_cont.add_child(procede_button)
	$_main_scroll_cont/_level1_v_cont.add_child(h_box_cont)
	
	var h_separetor = HSeparator.new()
	h_separetor.custom_minimum_size.y = 20.0
	h_separetor.modulate = Color("ffffff00")
	$_main_scroll_cont/_level1_v_cont.add_child(h_separetor)

func _creat_list():
	for child in $_main_scroll_cont/_level1_v_cont.get_children():
		child.queue_free()
	for i in range(0, len(manu_data["_menu"])):
		_build_list_unite(str(manu_data["_menu"][i]), i)
	var _creat_new_chapter = TextureButton.new()
	_creat_new_chapter.texture_normal = load("res://app_sections/menu/normal.png")
	_creat_new_chapter.texture_pressed = load("res://app_sections/menu/pressed_g13.png")
	_creat_new_chapter.stretch_mode = TextureButton.STRETCH_KEEP_ASPECT_CENTERED
	_creat_new_chapter.connect("pressed", _add_new_chapter)
	$_main_scroll_cont/_level1_v_cont.add_child(_creat_new_chapter)

func _creat_chapter():
	if FileAccess.file_exists(manu_save_path):
		if not manu_data["_menu"].has(str($creat_tab/_get_chapter.text)):
			manu_data["_menu"].append(str($creat_tab/_get_chapter.text))
			var file = FileAccess.open(manu_save_path, FileAccess.WRITE)
			var json_string = JSON.stringify(manu_data, "\t")
			file.store_string(json_string)
		else :
			pass
	
func _on__confirm_chapter_pressed():
	_creat_chapter()
	$AnimationPlayer.play("blinder_off")
	_creat_list()

func _add_new_chapter():
	$AnimationPlayer.play("blinder_on")
	pass

func _on__button_creat_new_chapter_pressed():
	_add_new_chapter()
	pass # Replace with function body.

extends Control


# Called when the node enters the scene tree for the first time.
func _ready():
	_check_menu_file()
	pass # Replace with function body.


var save_path = "res://data_center/menu_data/menu_data.json"

func _check_menu_file():
	if FileAccess.file_exists(save_path):
		$_new.disabled = true
		$_continue.disabled = false
	else :
		$_new.disabled = false
		$_continue.disabled = true

func _on__continue_pressed():
	get_tree().change_scene_to_file("res://app_sections/menu/menu.tscn")
	pass # Replace with function body.
func _on__new_pressed():
	get_tree().change_scene_to_file("res://app_sections/creat_new/creat_new.tscn")
	pass # Replace with function body.
